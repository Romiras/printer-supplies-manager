class Supply < ActiveRecord::Migration
  def self.up
    drop_table :supplies
  end

  def self.down
    create_table :supplies do |t|
      t.integer  "black"
      t.boolean  "iscolor"
      t.boolean  "haskit"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end
