class Type < ActiveRecord::Migration
  def self.up
    change_table :types do |t|
      t.rename :bgcolor, :qnt
      t.rename :name, :bgcolor
    end
  end

  def self.down
    change_table :types do |t|
      t.rename :bgcolor, :name
      t.rename :qnt, :bgcolor
    end
  end
end
