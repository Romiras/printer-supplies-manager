class Ink < ActiveRecord::Migration
  def self.up
    change_table :inks do |t|
      t.integer  "printer_id"
    end
  end

  def self.down
    change_table :inks do |t|
      t.remove  "printer_id"
    end
  end
end
