class CreateInks < ActiveRecord::Migration
  def self.up
    create_table :inks do |t|
      t.integer :cyan
      t.integer :magenta
      t.integer :yellow

      t.timestamps
    end
  end

  def self.down
    drop_table :inks
  end
end
