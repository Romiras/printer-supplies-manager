class Printer < ActiveRecord::Migration
  def self.up
    change_table :printers do |t|
      t.boolean  "haskit"
      t.boolean  "iscolor"
      t.integer  "black"
    end
  end

  def self.down
    change_table :printers do |t|
      t.remove  "haskit"
      t.remove  "iscolor"
      t.remove  "black"
    end
  end
end
