// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
function expMenu(id) {
  var itm = null;
  if (document.getElementById) {
    itm = document.getElementById(id);
  } else if (document.all){
    itm = document.all[id];
  } else if (document.layers){
    itm = document.layers[id];
  }
  if (itm) {
    if (itm.style) {
      if (itm.style.display == "none") {itm.style.display = "";}
      else {itm.style.display = "none";}
    }
    else {itm.visibility = "show";}
  }
}

function ShowHide(id1, id2) {
  if (id1 != '') expMenu(id1);
  if (id2 != '') expMenu(id2);
}

function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++)
      {
      Char = sText.charAt(i);
      if (ValidChars.indexOf(Char) == -1)
         {
         IsNumber = false;
         }
      }
   return IsNumber;
}

function validate_minimum_length(field, len)
{
  with (field)
  {
    if (value.length < len)
      {alert(field.name + " is too short (minimum is " + len + " characters)");return false;}
    else
      {return true;}
  }
}

function validate_is_number(field)
{
  with (field)
  {
    if (IsNumeric(value) == false)
      {alert(field.name + " is not a number");return false;}
    else
      {return true;}
  }
}

function validate_presence(field)
{
  with (field)
  {
    if (value==null||value=="")
      {alert(name + " can't be blank");return false;}
    else
      {return true;}
  }
}

function validate_form(thisform)
{
  with (thisform)
  {
    if (validate_presence(yellow) == false ||
	validate_is_number(yellow) == false) {yellow.focus();return false;}
    if (validate_presence(magenta) == false ||
	validate_is_number(magenta) == false) {magenta.focus();return false;}
    if (validate_presence(cyan) == false ||
	validate_is_number(cyan) == false) {cyan.focus();return false;}
    if (validate_presence(kit) == false ||
	validate_is_number(kit) == false) {kit.focus();return false;}
    return true;
  }
}

function bgstyle(qnt)
{
    if (qnt <= 1) {
      if (qnt == 0)
        {bgcolor = "red";}
      else
        {bgcolor = "orange";}
    } else {bgcolor = '';}
    return bgcolor
}

function set_element_bg(elmnt,qnt) {
    alert('grr');
    $(elmnt).setStyle({backgroundColor: bgstyle(qnt)});
}
