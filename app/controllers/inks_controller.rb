class InksController < ApplicationController
  before_filter :find_printer,
    :only => [:show, :edit]

  def show
  end

  def edit
  end

  def new
    @printer = Printer.find(params[:printer_id])
    @ink = Ink.new(params[:ink])
  end

  def create
    @ink = Ink.new(params[:ink])
    if @ink.save
      redirect_to printer_ink_path(@ink.printer)
    else
      render :action => "new"
    end
  end

  def update
    @printer = Printer.find(params[:printer_id])
    @ink = @printer.ink.build(params[:id])
    if @ink.update_attributes(params[:ink])
      redirect_to printer_ink_path(@ink.printer)
    else
      render :action => "edit"
    end
  end

  private
    def find_printer
      @printer = Printer.find(params[:printer_id])
      @ink = @printer.ink
    end
end
