require 'rubygems'
require 'fastercsv'

class PrintersController < ApplicationController
  in_place_edit_with_validation_for :printer

  # GET /printers
  # GET /printers.xml
  def index
    @printers = Printer.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @printers }
    end
  end

  # GET /printers/1
  # GET /printers/1.xml
  def show
    @printer = Printer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @printer }
    end
  end

  # GET /printers/new
  # GET /printers/new.xml
  def new
    @printer = Printer.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @printer }
    end
  end

  # GET /printers/1/edit
  def edit
    @printer = Printer.find(params[:id])
  end

  # POST /printers
  # POST /printers.xml
  def create
    @printer = Printer.new(params[:printer])
    @obj_id = Printer.find(:last).id
    respond_to do |format|
      if @printer.save
        if @printer.haskit?
          @kit = Kit.new
          @kit.qnt = params[:formkit][:qntkit]
          @printer.kit = @kit
          @kit.save
          #logger.debug "kit.qnt = #{params[:formkit][:qntkit]}"
        end
        if @printer.iscolor?
          @ink = Ink.new
          @ink.yellow = params[:formink][:yellow_ink]
          @ink.magenta = params[:formink][:magenta_ink]
          @ink.cyan = params[:formink][:cyan_ink]
          @printer.ink = @ink
          @ink.save
        end
        format.js
        format.html do
          flash[:notice] = 'Printer was successfully created.'
          redirect_to(@printer)
        end
        format.xml  { render :xml => @printer, :status => :created, :location => @printer }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @printer.errors, :status => :unprocessable_entity }
        format.js {
          render :update do |page|
            page.replace_html :question_errors, error_messages_for(:printer, :ink, :kit)
          end
        }
      end
    end
  end

  # PUT /printers/1
  # PUT /printers/1.xml
  def update
    @printer = Printer.find(params[:id])

    respond_to do |format|
      if @printer.update_attributes(params[:printer])
        flash[:notice] = 'Printer was successfully updated.'
        format.html { redirect_to(@printer) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @printer.errors, :status => :unprocessable_entity }
      end
    end
  end

  # Exports list of printers to CSV
  def export
    @printers = Printer.find(:all)
    name = params[:printer][:name]
    logger.debug "Name is '#{name}'"

    csv_string = FasterCSV.generate do |csv|
      @printers.each { |printer|
        csv << [printer.name]
      }
    end

    filename = name.downcase.gsub(/[^0-9a-z]/, "_") + ".csv"
    send_data(csv_string,
      :type => 'text/csv; charset=utf-8; header=present',
      :filename => filename)
  end

  # Imports list of printers from CSV
  def import
    @path = params[:printer][:path]
    
    rows = FasterCSV.read(@path)
    rows.each { |row|
      @printer = Printer.new
      @printer.name = row[1]
      @printer.black = 0
      @printer.save!
    }
  end

  # DELETE /printers/1
  # DELETE /printers/1.xml
  def destroy
    @printer = Printer.find(params[:id])
    @printer.destroy
    @obj_id = @printer.id

    respond_to do |format|
      format.html { redirect_to(printers_url) }
      format.js
      format.xml  { head :ok }
    end
  end
end
