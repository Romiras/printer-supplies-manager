class KitsController < ApplicationController
  before_filter :find_printer,
    :only => [:show, :edit]

  def show
  end

  def edit
  end

  def new
    @printer = Printer.find(params[:printer_id])
    @kit = Kit.new(params[:kit])
  end

  def create
    @kit = Kit.new(params[:kit])
    if @kit.save
      redirect_to printer_kit_path(@kit.printer)
    else
      render :action => "new"
    end
  end

  def update
    @printer = Printer.find(params[:printer_id])
    @kit = @printer.kit.build(params[:id])
    if @kit.update_attributes(params[:kit])
      redirect_to printer_kit_path(@kit.printer)
    else
      render :action => "edit"
    end
  end

  private
    def find_printer
      @printer = Printer.find(params[:printer_id])
      @kit = @printer.kit
    end
end
