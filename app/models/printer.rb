class Printer < ActiveRecord::Base
  validates_presence_of :name
  validates_length_of :name, :minimum => 3
  validates_numericality_of :black, :greater_than_or_equal_to => 0
  has_one :kit, :dependent => :destroy
  has_one :ink, :dependent => :destroy
end
