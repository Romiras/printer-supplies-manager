class Kit < ActiveRecord::Base
  validates_numericality_of :qnt, :greater_than_or_equal_to => 0
  belongs_to :printer
end
