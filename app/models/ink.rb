class Ink < ActiveRecord::Base
  validates_numericality_of :cyan, :greater_than_or_equal_to => 0
  validates_numericality_of :magenta, :greater_than_or_equal_to => 0
  validates_numericality_of :yellow, :greater_than_or_equal_to => 0
  belongs_to :printer
end
